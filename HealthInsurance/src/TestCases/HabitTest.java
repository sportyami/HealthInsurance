package TestCases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import bean.CurrentHealth;
import bean.Habits;
import bean.InsuredPerson;
import service.HabitPremium;

public class HabitTest {
	
	InsuredPerson per = new InsuredPerson();
	Habits habits = new Habits();
	HabitPremium habitPremium = new HabitPremium();
	
	@Test
	public void testWithoutHabits() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		float premiumValue = 5000f;
		per.setHabits(null);
		premiumValue = habitPremium.getPremium(premiumValue, per);
		assertEquals((float) 5000, premiumValue, 0.0f);
	}
	
	@Test
	public void testHabitsPremium() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		per.setGender("Male");
		float premiumValue = 5000f;
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		habits.setSmoking(false);
		habits.setDrugs(false);
		per.setHabits(habits);
		premiumValue = habitPremium.getPremium(premiumValue, per);
		assertEquals((float) 4995.5, premiumValue, 0.0f);
	}
	
}
