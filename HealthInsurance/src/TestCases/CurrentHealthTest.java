package TestCases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import bean.CurrentHealth;
import bean.Habits;
import bean.InsuredPerson;
import service.CurrentHealthPremium;

public class CurrentHealthTest {
	
	InsuredPerson per = new InsuredPerson();
	CurrentHealth currentHealth = new CurrentHealth();
	CurrentHealthPremium currentHealthPremium = new CurrentHealthPremium();
	
	@Test
	public void testWithoutHealth() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		float premiumValue = 5000f;
		per.setCurrentHealth(null);
		premiumValue = currentHealthPremium.getPremium(premiumValue, per);
		assertEquals((float) 5000, premiumValue, 0.0f);
	}
	
	@Test
	public void testCurrentHealthPremium() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		per.setGender("Male");
		float premiumValue = 5000f;
		currentHealth.setBloodPressure(false);
		currentHealth.setBloodSugar(false);
		currentHealth.setHypertension(false);
		currentHealth.setOverweight(true);
		per.setCurrentHealth(currentHealth);
		premiumValue = currentHealthPremium.getPremium(premiumValue, per);
		assertEquals((float) 5050, premiumValue, 0.0f);
	}

}
