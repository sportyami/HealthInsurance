package TestCases;

import org.junit.Test;
import static org.junit.Assert.*;

import bean.CurrentHealth;
import bean.Habits;
import bean.InsuredPerson;
import service.CalculatePremium;

public class TestCase {
	InsuredPerson per = new InsuredPerson();
	CurrentHealth currentHealth = new CurrentHealth();
	Habits habits = new Habits();
	CalculatePremium premium = new CalculatePremium();

	@Test
	public void testWithoutAge() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		float premiumValue = premium.getInsurancePremium(per);
		assertEquals((float) 5000, premiumValue, 0.0f);
	}

	@Test
	public void testAge() {
		per.setAge(34);
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		per.setGender("Male");
		float premiumValue = premium.getInsurancePremium(per);
		assertEquals((float) 6630, premiumValue, 0.0f);
	}

	@Test
	public void testPremium() {
		per.setAge(34);
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		per.setGender("Male");
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		habits.setSmoking(false);
		habits.setDrugs(false);
		per.setHabits(habits);
		currentHealth.setBloodPressure(false);
		currentHealth.setBloodSugar(false);
		currentHealth.setHypertension(false);
		currentHealth.setOverweight(true);
		per.setCurrentHealth(currentHealth);
		float premiumValue = premium.getInsurancePremium(per);
		assertEquals((float) 6690.2734, premiumValue, 0.0f);
	}

}
