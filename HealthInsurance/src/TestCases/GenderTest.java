package TestCases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import bean.InsuredPerson;
import service.GenderPremium;

public class GenderTest {
	
	InsuredPerson per = new InsuredPerson();
	GenderPremium genderPremium = new GenderPremium();

	
	@Test
	public void testGenderPremium() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		per.setGender("Male");
		float premiumValue = 5000f;
		premiumValue = genderPremium.getPremium(premiumValue, per);
		assertEquals((float) 5100, premiumValue, 0.0f);
	}
	
	@Test
	public void testWithoutGender() {
		per.setFirstName("Norman");
		per.setLastName("Gomes");
		float premiumValue = 5000f;
		premiumValue = genderPremium.getPremium(premiumValue, per);
		assertEquals((float) 5000, premiumValue, 0.0f);
	}

}
