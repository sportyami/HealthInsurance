package service;

import bean.InsuredPerson;
import interfaces.InsurancePremium;

public class CurrentHealthPremium implements InsurancePremium{

	/**
	 *  Method to calculate the premium by person current health
	 * @param premiumAmount
	 * @param person
	 * @return
	 */
	public float getPremium(float premiumAmount, InsuredPerson person) {
		if(null != person) {
			if (null != person.getCurrentHealth()) {
				if (person.getCurrentHealth().isHypertension()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.01);
				}
				if (person.getCurrentHealth().isBloodPressure()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.01);
				}
				if (person.getCurrentHealth().isBloodSugar()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.01);
				}
				if (person.getCurrentHealth().isOverweight()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.01);
				}
			}
		}
		System.out.println("Current Health Premium : Rs." + premiumAmount);
		return premiumAmount;
	}

}
