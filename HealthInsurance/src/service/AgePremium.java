package service;

import bean.InsuredPerson;
import interfaces.InsurancePremium;

public class AgePremium implements InsurancePremium{

	@Override
	public float getPremium(float premiumAmount, InsuredPerson person) {
		int basePremium = 5000;
		if(null != person) {
			if (person.getAge() < 18) {
				premiumAmount = 5000;
			}
			if (person.getAge() >= 18 && person.getAge() < 25) {
				premiumAmount = (float) (basePremium + (basePremium * 0.1));
			} else if (person.getAge() >= 25 && person.getAge() < 30) {
				premiumAmount = (float) (basePremium + (basePremium * 0.1) * 2);
			} else if (person.getAge() >= 30 && person.getAge() < 35) {
				premiumAmount = (float) (basePremium + (basePremium * 0.1) * 3);
			} else if (person.getAge() >= 35 && person.getAge() < 40) {
				premiumAmount = (float) (basePremium + (basePremium * 0.1) * 4);
			} else if (person.getAge() >= 40) {
				premiumAmount = (float) (basePremium + (basePremium * 0.1) * 4 + (basePremium * 0.2));
			}
		}
		System.out.println("Age Premium : Rs." + premiumAmount);
		return premiumAmount;
	}

}
