package service;

import bean.InsuredPerson;
import interfaces.InsurancePremium;

public class HabitPremium implements InsurancePremium{
	
	/**
	 *  Method to calculate the premium by person habit
	 * @param premiumAmount
	 * @param person
	 * @return
	 */
	public float getPremium(float premiumAmount, InsuredPerson person) {
		if(null != person) {
			if (null != person.getHabits()) {
				if (person.getHabits().isAlcohol()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.03);
				}
				if (person.getHabits().isDailyExercise()) {
					premiumAmount = (float) (premiumAmount - premiumAmount * 0.03);
				}
				if (person.getHabits().isDrugs()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.03);
				}
				if (person.getHabits().isSmoking()) {
					premiumAmount = (float) (premiumAmount + premiumAmount * 0.03);
				}
			}
		}
		System.out.println("Habit Premium : Rs." + premiumAmount);
		return premiumAmount;
	}
}
