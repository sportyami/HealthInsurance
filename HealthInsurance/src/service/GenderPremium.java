package service;

import bean.InsuredPerson;
import interfaces.InsurancePremium;

public class GenderPremium implements InsurancePremium{

	/**
	 *  Method to calculate the premium by gender of person
	 * @param premiumAmount
	 * @param person
	 * @return
	 */
	public float getPremium(float premiumAmount, InsuredPerson person) {
		if(null != person) {
			if (null != person.getGender() && person.getGender().equals("Male")) {
				premiumAmount = (float) (premiumAmount + premiumAmount * 0.02);
			}
		}
		System.out.println("Gender Premium : Rs." + premiumAmount);
		return premiumAmount;
	}
}
