package interfaces;

import bean.InsuredPerson;

public interface InsurancePremium {
	
	public float getPremium(float premiumAmount, InsuredPerson person);

}
